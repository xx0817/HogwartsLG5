# -*- coding: utf-8 -*-
# @Time    : 2021/4/26 9:44
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : calculator.py

class Calculator:

    def add(self, a, b):
        return a + b

    def sub(self, a, b):
        return a - b

    def mul(self, a, b):
        return a * b

    def div(self, a, b):
        return a / b