# -*- coding: utf-8 -*-
# @Time    : 2021/5/10 16:07
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : contactedit_page.py


# 编辑成员的信息
from time import sleep

from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from MobileTestAppium.wework_po.page.base_page import BasePage
from MobileTestAppium.wework_po.page.memberinvite_page import MemberInvitePage


class ContactEditPage(BasePage):

    def edit_name(self, name):

        self.driver.find_element(MobileBy.XPATH, "//*[contains(@text,'姓名')]/../android.widget.EditText").send_keys(name)
        return self

    def edit_gender(self, gender):

        locator = (MobileBy.XPATH, "//*[@text='男']")
        ele = WebDriverWait(self.driver, 6).until(
            EC.element_to_be_clickable(locator))
        ele.click()
        # sleep(1)
        if gender == '女':
            self.driver.find_element(MobileBy.XPATH, "//*[@text='女']").click()
        else:
            self.driver.find_element(MobileBy.XPATH, "//*[@text='男']").click()
        return self

    def edit_phonenum(self, phonenum):

        self.driver.find_element(MobileBy.ID, "com.tencent.wework:id/f4m").send_keys(phonenum)
        return self

    def click_save(self):

        self.driver.find_element(MobileBy.ANDROID_UIAUTOMATOR,
                                 'new UiScrollable(new UiSelector().'
                                 'scrollable(true).instance(0)).'
                                 'scrollIntoView(new UiSelector().'
                                 'text("保存").instance(0));').click()
        return MemberInvitePage(self.driver)
