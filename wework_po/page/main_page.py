# -*- coding: utf-8 -*-
# @Time    : 2021/5/10 15:57
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : main_page.py


# 点击通讯录
from appium.webdriver.common.mobileby import MobileBy

from MobileTestAppium.wework_po.page.addresslist_page import AddressListPage
from MobileTestAppium.wework_po.page.base_page import BasePage


class MainPage(BasePage):

    def addresslist(self):

        self.driver.find_element(MobileBy.XPATH, '//*[@text="通讯录"]').click()
        return AddressListPage(self.driver)