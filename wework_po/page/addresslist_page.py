# -*- coding: utf-8 -*-
# @Time    : 2021/5/10 16:00
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : addresslist_page.py
from appium.webdriver.common.mobileby import MobileBy

from MobileTestAppium.wework_po.page.base_page import BasePage
from MobileTestAppium.wework_po.page.memberinvite_page import MemberInvitePage


class AddressListPage(BasePage):

    def add_member(self):

        self.driver.find_element(MobileBy.ID, 'com.tencent.wework:id/h8l').click()
        self.driver.find_element(MobileBy.XPATH, '//*[@text="添加成员"]').click()
        return MemberInvitePage(self.driver)