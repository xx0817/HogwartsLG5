# -*- coding: utf-8 -*-
# @Time    : 2021/5/10 16:23
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : base_page.py
from selenium.webdriver.remote.webdriver import WebDriver


class BasePage():

    def __init__(self, driver: WebDriver = None):

        self.driver = driver