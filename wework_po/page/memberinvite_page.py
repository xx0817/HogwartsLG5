# -*- coding: utf-8 -*-
# @Time    : 2021/5/10 16:03
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : memberinvite_page.py


# 点击手动添加
from appium.webdriver.common.mobileby import MobileBy

from MobileTestAppium.wework_po.page.base_page import BasePage
from MobileTestAppium.wework_po.page.contactedit_page import ContactEditPage


class MemberInvitePage(BasePage):

    def addconect_menual(self):

        self.driver.find_element(MobileBy.XPATH, '//*[@text="手动输入添加"]').click()
        return ContactEditPage(self.driver)

    def get_toast(self):

        ele = self.driver.find_element(MobileBy.XPATH, '//*[@class="android.widget.Toast"]').text
        return ele