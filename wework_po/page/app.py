# -*- coding: utf-8 -*-
# @Time    : 2021/5/10 15:55
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : app.py


# 启动app、关闭app、重启app、进入首页...
from appium import webdriver

from MobileTestAppium.wework_po.page.base_page import BasePage
from MobileTestAppium.wework_po.page.main_page import MainPage


class App(BasePage):

    def start(self):

        if self.driver == None:
            caps = {}
            caps["platformName"] = "Android"
            caps["deviceName"] = "wework"
            caps["appPackage"] = "com.tencent.wework"
            caps["appActivity"] = ".launch.LaunchSplashActivity"
            # 不清空本地缓存，启动app
            caps["noReset"] = "true"
            caps["ensureWebviewsHavePages"] = True
            # 设置页面等待空闲状态的时间为2秒
            caps['settings[waitForIdleTimeout]'] = 2

            self.driver = webdriver.Remote("http://localhost:4723/wd/hub", caps)
            self.driver.implicitly_wait(6)
        else:
            self.driver.launch_app()
        return self

    def restart(self):

        self.driver.quit()
        self.driver.launch_app()
        return self

    def stop(self):

        self.driver.quit()
        return self

    def goto_main(self):

        return MainPage(self.driver)