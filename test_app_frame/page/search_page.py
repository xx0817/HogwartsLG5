# -*- coding: utf-8 -*-
# @Time    : 2021/5/11 17:34
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : search_page.py
from MobileTestAppium.test_app_frame.base_page import BasePage


class SearchPage(BasePage):

    def search(self):
        self.run_steps("../data/search.yaml", "search")
        return True