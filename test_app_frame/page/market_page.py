# -*- coding: utf-8 -*-
# @Time    : 2021/5/11 17:26
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : market_page.py

from MobileTestAppium.test_app_frame.page.search_page import SearchPage
from MobileTestAppium.test_app_frame.base_page import BasePage


class MarketPage(BasePage):

    def goto_search(self):
        self.run_steps("../data/market.yaml", "goto_search")
        return SearchPage(self.driver)