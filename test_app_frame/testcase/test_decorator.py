# -*- coding: utf-8 -*-
# @Time    : 2021/5/15 13:44
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : test_decorator.py

# 装饰器演示用法
def b(fun_123455):
    def run_1244215(*args, **kwargs):
        print("before a")
        fun_123455(*args, **kwargs)
        print("after a")
    return run_1244215

@b
def a():
    print('a')


def test_demo():
    a()