# -*- coding: utf-8 -*-
# @Time    : 2021/5/11 17:56
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : test_main.py
from MobileTestAppium.test_app_frame.app import App


class TestSearch():

    def test_search(self):
        self.app = App()
        result = self.app.start().goto_main().goto_market_page().goto_search().search()
        assert result

    def test_mine(self):
        self.app = App()
        self.app.start().goto_main().goto_mine()