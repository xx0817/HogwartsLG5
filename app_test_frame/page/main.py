# -*- coding: utf-8 -*-
# @Time    : 2021/5/11 17:21
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : main.py
from MobileTestAppium.app_frame.base_page import BasePage
from MobileTestAppium.app_frame.page.market_page import MarketPage


class Main(BasePage):

    def goto_market_page(self):
        self.run_steps("../data/main.yaml", "goto_market_page")
        return MarketPage(self.driver)