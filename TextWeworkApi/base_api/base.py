
import requests


class Base():

    def __init__(self):
        # 获取token
        self.id = ''
        self.secret = ''
        self.url = f'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={self.id}&corpsecret={self.secret}'
        self.token = requests.get(self.url).json()['access_token']
        self.s = requests.Session()
        self.s.params = {
            'access_token': self.token
        }

    def send(self, *args, **kwargs):
        r = self.s.request(*args, **kwargs)
        return r.json()