# -*- coding: utf-8 -*-
# @Time    : 2021/5/18 15:09
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : take_contact.py
from TextWeworkApi.base_api.base import Base


class TakeContace(Base):

    #添加成员
    def add_member(self, userid, name, mobile, department, email, **kwargs):

        url = 'https://qyapi.weixin.qq.com/cgi-bin/user/create?'
        data = {
            "userid": userid,
            "name": name,
            "mobile": mobile,
            "department": department,
            "email": email
        }
        # 字典更新操作，如果原字典中没有key-value对，则增加，如果有，则更新key-value
        data.update(kwargs)
        return self.send("post", url, json=data)

    # 获取成员信息
    def get_member(self, userid):
        url = f"https://qyapi.weixin.qq.com/cgi-bin/user/get?userid={userid}"
        return self.send("get", url)

    # 修改成员信息
    def update_member(self, userid, newname, **kwargs):
        url = f"https://qyapi.weixin.qq.com/cgi-bin/user/update?"
        new_data = {
            "userid": f"{userid}",
            "name": newname, }
        new_data.update(kwargs)
        return self.send("post", url, json=new_data)

    # 删除成员
    def delete_member(self, userid: str):
        url = f"https://qyapi.weixin.qq.com/cgi-bin/user/delete?userid={userid}"
        return self.send("get", url)

