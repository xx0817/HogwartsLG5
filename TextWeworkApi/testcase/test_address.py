# -*- coding: utf-8 -*-
# @Time    : 2021/5/18 15:47
# @Author  : Yan
# @Email   : 13433183608@163.com
# @File    : test_address.py
from TextWeworkApi.base_api.take_contact import TakeContace


class TestAddress:

    def setup(self):
        self.address = TakeContace()

    def test_add_member(self):
        # 清理数据
        res = self.address.delete_member(userid="zhangsan")
        assert res["errcode"] == 0


        # 查询成员，清理验证
        res = self.address.get_member(userid="zhangsan")
        assert res["errcode"] == 60111

        # 创建成员
        res = self.address.add_member(userid="zhangsan", name="张三", mobile="+86 13800000002", department=[1])
        assert res["errcode"] == 0
        # 查询成员,创建验证
        res = self.address.get_member(userid="zhangsan")
        assert res["errcode"] == 0

        # 更新成员
        res = self.address.update_member(userid="zhangsan", newname="李四")
        assert res["errcode"] == 0
        # 查询成员，更新验证
        res = self.address.get_member(userid="zhangsan")
        assert res["errcode"] == 0

