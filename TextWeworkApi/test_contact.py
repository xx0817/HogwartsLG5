

import requests


class TestContact:

    #获取token
    def test_get_token(self):
        id = ''
        secret = ''
        url = f'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={id}&corpsecret={secret}'
        r = requests.get(url)
        return r.json()['access_token']

    #添加成员
    def test_add_member(self):
        url = f'https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token={self.test_get_token()}'
        body = {
            "userid": "xiaowu",
            "name": "小舞",
            "mobile": "+86 13800000810",
            "department": [1],
            "email": "xiaowu@hongyu.com"
        }
        r = requests.post(url, json=body)
        assert r.json()["errcode"] == 0

    #删除成员
    def test_delete_member(self):
        id = 'xiaowu'
        url = f'https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token={self.test_get_token()}&userid={id}'
        r = requests.get(url)
        assert r.json()["errcode"] == 0

    #修改成员信息
    def test_update_member(self):
        url = f'https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token={self.test_get_token()}'
        body = {
            "userid": "xiaowu",
            "name": "唐三老婆",
        }
        r = requests.post(url, json=body)
        assert r.json()["errcode"] == 0

    #获取成员信息
    def test_get_contact(self):
        id = 'xiaowu'
        url = f'https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={self.test_get_token()}&userid={id}'
        r = requests.get(url)
        assert r.json()["errcode"] == 0